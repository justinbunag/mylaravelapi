<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


// List Articles
Route::get('articles','ArticlesController@index');

// List Single Articles

Route::get('article/{id}','ArticlesController@show');

//Creeate new Article 

Route::post('article','ArticlesController@store');
// Update Article

Route::put('article','ArticleController@store');

//Delete Article 

Route::delete('article/{id}','ArticlesController@destroy');
